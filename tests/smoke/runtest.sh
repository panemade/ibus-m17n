#!/bin/bash
. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart
    rlPhaseStartSetup
        rlRun "tmp=\$(mktemp -d)" 0 "Create tmp directory"
        rlRun "pushd $tmp"
        rlFetchSrcForInstalled "ibus-m17n"
        rlRun "rpm --define '_topdir $tmp' -i *src.rpm"
        rlRun "mkdir BUILD" 0 "Creating BUILD directory"
        rlRun "rpmbuild --nodeps --define '_topdir $tmp' -bp $tmp/SPECS/*spec"
        rlRun "pushd BUILD/ibus-m17n*"
        rlRun "set -o pipefail"
        rlRun "NOCONFIGURE=1 ./autogen.sh"
        rlRun "./configure --disable-static --with-gtk=3.0"
        rlRun "make check"
        rlAssertRpm "ibus"
        rlAssertRpm "m17n-db"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "ibus-daemon -v -r -d"
        rlRun "sleep 5" 0 "Give ibus-daemon some time to start properly."
        for name in \
            am:sera \
            ar:kbd \
            ar:translit \
            as:inscript \
            as:inscript2 \
            as:itrans \
            as:phonetic \
            ath:phonetic \
            bla:phonetic \
            bn:disha \
            bn:inscript \
            bn:inscript2 \
            bn:itrans \
            bn:probhat \
            bo:ewts \
            bo:tcrc \
            bo:wylie \
            brx:inscript2 \
            cr:western \
            da:post \
            doi:inscript2 \
            dv:phonetic \
            eo:h-fundamente \
            eo:h-sistemo \
            eo:plena \
            eo:q-sistemo \
            eo:vi-sistemo \
            eo:x-sistemo \
            fa:isiri \
            fr:azerty \
            grc:mizuochi \
            gu:inscript \
            gu:inscript2 \
            gu:itrans \
            gu:phonetic \
            hi:inscript \
            hi:inscript2 \
            hi:itrans \
            hi:optitransv2 \
            hi:phonetic \
            hi:remington \
            hi:typewriter \
            hi:vedmata \
            hu:rovas-post \
            ii:phonetic \
            iu:phonetic \
            kk:arabic \
            km:yannis \
            kn:inscript \
            kn:inscript2 \
            kn:itrans \
            kn:kgp \
            kn:optitransv2 \
            kn:typewriter \
            kok:inscript2 \
            ks:inscript \
            ks:kbd \
            lo:lrt \
            mai:inscript \
            mai:inscript2 \
            ml:enhanced-inscript \
            ml:inscript \
            ml:inscript2 \
            ml:itrans \
            ml:mozhi \
            ml:remington \
            ml:swanalekha \
            mni:inscript2-beng \
            mni:inscript2-mtei \
            mr:inscript \
            mr:inscript2 \
            mr:itrans \
            mr:phonetic \
            mr:remington \
            mr:typewriter \
            ne:inscript2 \
            ne:rom \
            ne:rom-translit \
            ne:trad \
            ne:trad-ttf \
            nsk:phonetic \
            oj:phonetic \
            or:inscript \
            or:inscript2 \
            or:itrans \
            or:phonetic \
            pa:anmollipi \
            pa:inscript \
            pa:inscript2 \
            pa:itrans \
            pa:jhelum \
            pa:phonetic \
            ps:phonetic \
            ru:phonetic \
            ru:translit \
            ru:yawerty \
            sa:IAST \
            sa:harvard-kyoto \
            sa:inscript2 \
            sa:itrans \
            sat:inscript2-deva \
            sat:inscript2-olck \
            sd:inscript \
            sd:inscript2 \
            si:phonetic-dynamic \
            si:samanala \
            si:sayura \
            si:singlish \
            si:sumihiri \
            si:transliteration \
            si:wijesekera \
            sv:post \
            t:latn-post \
            t:latn-pre \
            t:latn1-pre \
            t:lsymbol \
            t:math-latex \
            t:rfc1345 \
            t:ssymbol \
            t:syrc-phonetic \
            t:unicode \
            ta:inscript \
            ta:inscript2 \
            ta:itrans \
            ta:lk-renganathan \
            ta:phonetic \
            ta:tamil99 \
            ta:typewriter \
            ta:vutam \
            tai:sonla-kbd \
            te:apple \
            te:inscript \
            te:inscript2 \
            te:itrans \
            te:pothana \
            te:rts \
            te:sarala \
            th:kesmanee \
            th:pattachote \
            th:tis820 \
            ur:phonetic \
            vi:han \
            vi:nomtelex \
            vi:nomvni \
            vi:tcvn \
            vi:telex \
            vi:viqr \
            vi:vni \
            yi:yivo
        do
            rlRun "/usr/libexec/ibus-engine-m17n --xml 2>/dev/null | grep '<name>m17n:${name}</name>'" \
                0 "checking whether 'ibus-engine-m17n --xml' can list m17n:${name}:"
            rlRun "ibus list-engine --name-only | grep 'm17n:${name}$'" \
                0 "checking whether ibus can list m17n:${name}:"
        done
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "popd; popd"
        rlRun "rm -r $tmp" 0 "Remove tmp directory"
    rlPhaseEnd
rlJournalEnd
